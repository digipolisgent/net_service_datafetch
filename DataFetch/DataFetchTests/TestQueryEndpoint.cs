using System;
using Xunit;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Hosting;
using DataFetch;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace DataFetch.Tests
{
    public class TestQueryEndpoint
    {

        private readonly TestServer _server;
        private readonly HttpClient _client;

        public TestQueryEndpoint()
        {
            //TODO: Set up database

            // Get configuration.
            var configuration = new ConfigurationBuilder()
            .SetBasePath(Path.GetFullPath(@"../../../../DataFetch/"))
            .AddJsonFile("appsettings.json", optional: false)
            .Build();

            // Arrange
            _server = new TestServer(new WebHostBuilder()
                .UseStartup<Startup>()
                .UseConfiguration(configuration));
            _client = _server.CreateClient();
        }

        [Fact]
        public async Task GetAllWithoutPaging()
        {
            var response = await _client.GetAsync("/fetch/sms");
            response.EnsureSuccessStatusCode();
            var body = await response.Content.ReadAsStringAsync();
            var json = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(body);
            Assert.True(json.ToArray().Length > 1);
        }

        [Fact]
        public async Task GetAllWithOffset()
        {
            var response = await _client.GetAsync("/fetch/sms");
            var body = await response.Content.ReadAsStringAsync();
            var jsonWithoutOffset = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(body);

            response = await _client.GetAsync("/fetch/sms?$offset=2");
            response.EnsureSuccessStatusCode();
            body = await response.Content.ReadAsStringAsync();
            var jsonWithOffset = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(body);
            Assert.NotEqual(jsonWithoutOffset[0]["id"], jsonWithOffset[1]["id"]);
        }

        [Fact]
        public async Task GetAllWithLimit()
        {
            var response = await _client.GetAsync("/fetch/sms?$limit=1");
            response.EnsureSuccessStatusCode();
            var body = await response.Content.ReadAsStringAsync();
            var json = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(body);
            Assert.Equal(json.ToArray().Length, 1);
        }

        [Fact]
        public async Task GetAllWithOrderBy()
        {
            var response = await _client.GetAsync("/fetch/sms");
            var body = await response.Content.ReadAsStringAsync();
            var jsonWithoutOffset = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(body);

            response = await _client.GetAsync("/fetch/sms?orderBy=id%20DESC");
            response.EnsureSuccessStatusCode();
            body = await response.Content.ReadAsStringAsync();
            var jsonWithOffset = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(body);
            Assert.NotEqual(jsonWithoutOffset[0]["id"], jsonWithOffset[1]["id"]);
        }

    }
}
