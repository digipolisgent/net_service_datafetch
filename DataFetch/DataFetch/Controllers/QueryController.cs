﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using DataFetch.Data;
using System.Net;

namespace DataFetch.Controllers
{
    [Route("/fetch")]
    public class QueryController : Controller
    {
        public IOptions<ConnectionConfig> config;

        public IDataFetcher dataFetcher;

        public QueryController(IOptions<ConnectionConfig> config, IDataFetcher dataFetcher)
        {
            this.dataFetcher = dataFetcher;
            this.config = config;
        }

        
        [HttpGet("{query}")]
        public IActionResult Get(string query)
        {
            try
            {
                return Json(dataFetcher.ExecuteQuery(queryName: query, parameters: Request.Query));
            }
            catch (ParameterMissingException e)
            {
                JsonResult result = Json(new { error = e.Message });
                result.StatusCode = (int) HttpStatusCode.BadRequest;
                return result;
            }
            catch (ResourceDoesNotExistException e)
            {
                JsonResult result = Json(new { error = e.Message });
                result.StatusCode = (int) HttpStatusCode.NotFound;
                return result;
            }

        }
    }
}
