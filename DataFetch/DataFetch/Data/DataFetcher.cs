﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace DataFetch.Data
{
    public class DataFetcher : IDataFetcher
    {
        public IOptions<ConnectionConfig> config;
        public IConfiguration generalConfig;

        public DataFetcher(IOptions<ConnectionConfig> config, IConfiguration generalConfig)
        {
            this.config = config;
            this.generalConfig = generalConfig;
        }
        public List<Dictionary<string, object>> ExecuteQuery(String queryName, IQueryCollection parameters)
        {
            List<Dictionary<string, object>> records = new List<Dictionary<string, object>>();

            using (SqlConnection con = new SqlConnection(config.Value.ConnectionString))
            {
                con.Open();

                IConfigurationSection queries = generalConfig.GetSection("queries");
                if (queries == null || queries.GetValue<string>(queryName) == null)
                {
                    throw new ResourceDoesNotExistException("Resource does not exist: " + queryName);
                }

                String query = queries.GetValue<string>(queryName).Replace(";", "");
                if (HasPaging(parameters))
                {
                    query = WrapWithPagingParameters(query, parameters);
                }
                               
                using (SqlCommand command = new SqlCommand(query, con))
                {
                    foreach (string key in parameters.Keys)
                    {
                        if (new String[] { "$limit", "$orderBy", "$offset" }.Contains(key))
                            continue;
                        command.Parameters.AddWithValue("@" + key, parameters[key].ToString());
                    }

                    FillInPagingDefaults(parameters, command);
                    try
                    {
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.HasRows && reader.Read())
                        {
                            Dictionary<string, object> record = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.GetName(i) == "Row")
                                {
                                    continue;
                                }
                                //TEMPLATE: If you want to map the data to your own data structure,
                                //          this would be the place to do it! Create your objects
                                //          and fill them in with the data from the SqlDataReader.
                                record.Add(reader.GetName(i), reader.GetValue(i));
                            }
                            records.Add(record);
                        }

                    }
                    catch (SqlException se)
                    {
                        if (se.Message.Contains("scalar variable"))
                        {
                            throw new ParameterMissingException("Parameter missing: " +
                                se.Message.Substring(se.Message.IndexOf("\"")).Replace("\"", ""));
                        }
                        throw se;
                    }
                }
            }
            return records;
        }

        private void FillInPagingDefaults(IQueryCollection parameters, SqlCommand command)
        {
            long offset = 0;
            long limit = 65535;

            if (parameters.ContainsKey("$offset"))
            {
                offset = long.Parse(parameters["$offset"]);
            }
            if (parameters.ContainsKey("$limit"))
            {
                limit = long.Parse(parameters["$limit"]);
            }

            if (HasPaging(parameters))
            {
                command.Parameters.Add("limit", SqlDbType.BigInt).Value = offset + limit;
                command.Parameters.Add("offset", SqlDbType.BigInt).Value = offset;
            }
        }

        private string WrapWithPagingParameters(string query, IQueryCollection parameters)
        {
            string orderBy = "id";
            if (parameters.ContainsKey("$orderBy"))
            {
                orderBy = parameters["$orderBy"];
            }

            return "SELECT * FROM  (SELECT  ROW_NUMBER() OVER(ORDER BY " + orderBy + " ) AS Row, " +
                    query.Replace("SELECT ", " ") + ") AS tbl WHERE  Row >= @offset AND Row <= @limit;";
        }

        private bool HasPaging(IQueryCollection parameters)
        {
            return parameters.ContainsKey("$offset") || parameters.ContainsKey("$limit") || parameters.ContainsKey("$orderBy");
        }
    }
}
