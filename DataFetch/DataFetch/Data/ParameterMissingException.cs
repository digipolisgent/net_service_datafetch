﻿using System;
using System.Runtime.Serialization;

namespace DataFetch.Data
{
    [Serializable]
    internal class ParameterMissingException : Exception
    {
        public ParameterMissingException()
        {
        }

        public ParameterMissingException(string message) : base(message)
        {
        }

        public ParameterMissingException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ParameterMissingException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}