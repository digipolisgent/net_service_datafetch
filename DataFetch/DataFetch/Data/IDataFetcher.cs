﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace DataFetch.Data
{
    public interface IDataFetcher
    {
        List<Dictionary<string, object>> ExecuteQuery(String queryName, IQueryCollection parameters);
    }
}
