﻿using System;
using System.Runtime.Serialization;

namespace DataFetch.Data
{
    [Serializable]
    internal class ResourceDoesNotExistException : Exception
    {
        public ResourceDoesNotExistException()
        {
        }

        public ResourceDoesNotExistException(string message) : base(message)
        {
        }

        public ResourceDoesNotExistException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ResourceDoesNotExistException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}